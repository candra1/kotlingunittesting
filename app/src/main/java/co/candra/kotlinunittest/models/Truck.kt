package co.candra.kotlinunittest.models

data class Truck(var name: String = "", var driver: Driver= Driver(),var cargo:Cargo = Cargo(),
                 var destination: Position = Position(), var currentPosition: Position= Position()
) {
    fun load(cargo: Cargo) {
        this.cargo = cargo
    }
    fun driveToDestination(){
        this.currentPosition = this.destination
    }
}