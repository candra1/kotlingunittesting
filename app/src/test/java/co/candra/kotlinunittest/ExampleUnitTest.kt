package co.candra.kotlinunittest

import co.candra.kotlinunittest.models.Cargo
import co.candra.kotlinunittest.models.Driver
import co.candra.kotlinunittest.models.Position
import co.candra.kotlinunittest.models.Truck
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun shouldDeliverCargoToDestination() {
        // given
        val driver = Driver("Teddy")
        val cargo = Cargo()
        val destinationPosition = Position("52.229676", "21.012228")
        val truck = Truck()
        truck.driver = driver
        truck.load(cargo)
        truck.destination = destinationPosition

        // when
        truck.driveToDestination()

        // then
        assertEquals(destinationPosition, truck.currentPosition)
    }
}